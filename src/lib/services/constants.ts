import BitcoinSvelte from '../icons/Bitcoin.svelte';
import BNBSvelte from '../icons/BNB.svelte';
import EthereumSvelte from '../icons/Ethereum.svelte';

export const assets = [
	{
		name: 'BTC',
		chain: 'Bitcoin',
		assetCode: 'BTC.BTC',
		swapURL: 'https://app.thorswap.finance/swap/BTC.BTC_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/BTC.BTC',
		color: 'text-orange',
		icon: BitcoinSvelte,
	},
	{
		name: 'ETH',
		chain: 'Ethereum',
		assetCode: 'ETH.ETH',
		swapURL: 'https://app.thorswap.finance/swap/ETH.ETH_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/ETH.ETH',
		color: 'text-slate-400',
		icon: EthereumSvelte,
	},
	{
		name: 'BNB',
		chain: 'Binance Chain',
		assetCode: 'BNB.BNB',
		swapURL: 'https://app.thorswap.finance/swap/BNB.BNB_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/BNB.BNB',
		color: 'text-yellow-400',
		icon: BNBSvelte,
	},
];

export const totalSupply = 500000000;

export const appsAndServices = [
	{
		title: 'ASGARDEX',
		url: 'https://github.com/thorchain/asgardex-electron',
		logo: '/images/services/asgardex.png',
		description:
			'ASGARDEX is a no-fee, standalone desktop app implementing THORChain-based decentralized swaps. As the only open-source frontend codebase, it serves as a reference for many developers building in the ecosystem.',
		tags: ['Exchange']
	},
	{
		title: 'THORSwap',
		url: 'https://app.thorswap.finance/',
		logo: '/images/services/thorswap.png',
		description:
			'THORswap is a multi-chain DEX aggregator built on THORChain\'s cross-chain liquidity protocol and the leading interface for all THORChain services like THORNames and synthetic assets.',
		tags: ['Exchange'],
	},
	{
		title: 'XDEFI',
		url: 'https://www.xdefi.io/',
		logo: '/images/services/xdefi.png',
		description:
			'XDEFI is a multichain wallet that allows you to securely store, swap, and send Crypto and NFTs across 14 blockchains.',
		tags: ['Wallet', 'Exchange']
	},
	{
		title: 'ThorWallet',
		url: 'https://thorwallet.app.link/4MoUX7GFzmb',
		logo: '/images/services/thorwallet.svg',
		description:
			'THORWallet DEX is a mobile DeFi wallet. It\'s a non-custodial wallet with integrated cross-chain Dex. You can swap native tokens and manage your liquidity positions on THORChain, on/off ramp fiat, and more.',
		tags: ['Wallet', 'Exchange']
	},
	{
		title: 'Trust Wallet',
		url: 'https://trustwallet.com',
		logo: '/images/services/trustwallet.svg',
		description:
			'Trust Wallet is a self-custody wallet on iOS and Android that supports native RUNE and cross-chain swaps using THORChain.',
		tags: ['Wallet']
	},
	{
		title: 'Viewblock',
		url: 'https://viewblock.io/thorchain',
		logo: '/images/services/viewblock.png',
		description:
			'Viewblock is a block explorer that lets users explore, filter, and query blocks confirmed on the THORChain network.',
		tags: ['Explorer']
	},
	{
		title: 'Thorchain Explorer',
		url: 'https://thorchain.net',
		logo: '/images/services/Thorcircle.png',
		description:
			'THORChain Block Explorer and Dashboard. Explore meaningful THORChain data in one simple interface.',
		tags: ['Analytics', 'Explorer']
	},
	{
		title: 'Edge Wallet',
		url: 'https://edge.app/',
		logo: '/images/services/edge.svg',
		description:
			'Edge is an industry leading self-custody wallet and exchange that supports the most popular cryptocurrencies on the market.',
		tags: ['Wallet'],
	},
	{
		title: 'ShapeShift DAO',
		url: 'https://shapeshift.com/',
		logo: '/images/services/shapeshiftdao.svg',
		description:
			'ShapeShift is a pioneer in self-custody for digital asset trading. Our open source platform empowers users to safely buy, hold, trade, and invest in a range of digital assets and improves access to open, decentralized financial systems.',
		tags: ['Exchange', 'Community']
	},
	{
		title: 'Rango Exchange',
		url: 'https://app.rango.exchange/swap/BSC.BNB/AVAX_CCHAIN.AVAX',
		logo: '/images/services/rango.png',
		description:
			'Rango Exchange a powerful multi-chain aggregator for DEX and bridges, based on reachability and support of top blockchains. Rango will be able to find the most secure, fast, and easy path for it. Currently, Rango supports more than dozens of blockchains, 12+ bridges/DEXes, and 7+ different wallets, with a modern and user-friendly UX in the market.',
		tags: ['Exchange'],
	},
	{
		title: 'Defispot',
		url: 'https://defispot.com',
		logo: '/images/services/defispot.svg',
		description:
			'Defispot is an all in one multi-chain DEX built on THORChain, that brings usability to DeFi for all users by offering a traditional UI/UX- a DEX disguised as a CEX.',
		tags: ['Exchange']
	},
	{
		title: 'Ledger',
		url: 'https://support.ledger.com/hc/en-us/articles/4402987997841-THORChain-RUNE-?support=true',
		logo: '/images/services/ledger.jpeg',
		description: 'Ledger is a popular multichain hardware wallet that supports native RUNE. Ledger is supported by many exchanges that use THORChain.',
		tags: ['Wallet'],
	},
	{
		title: 'Ferz Wallet',
		url: 'https://ferz.com',
		logo: '/images/services/ferz.svg',
		description:
			'Ferz is an iOS, Android, and browser extension-based multichain Wallet and Exchange built with THORChain.',
		tags: ['Wallet', 'Exchange']
	},
	{
		title: 'Liquality',
		url: 'https://liquality.io/',
		logo: '/images/services/liquality.svg',
		description: 'Liquality is a multichain wallet that supports native RUNE.',
		tags: ['Wallet', 'Exchange'],
	},
	{
		title: 'Decentralfi',
		url: 'https://decentralfi.io/',
		logo: '/images/services/decentralfi.svg',
		description:
			'Decentralfi is a decentralized exchange built with THORChain. Decentralfi has a slippage optimization bot named Slippy to prompt users to break up large trades to optimize swap fees.',
		tags: ['Exchange']
	},
	 {
	 	title: 'Lends',
	 	url: 'https://lends.so/',
	 	logo: '/images/services/lendscape.svg',
	 	description:
	 		'Lends (rebranded from Lendscape) is a decentralized exchange utilizing THORChain with plans to support lending.',
	 	tags: ['Exchange']
	 },
	{
		title: 'THORNode.network',
		url: 'https://thornode.network/',
		logo: '/images/services/Thorcircle.png',
		description:
			'THORChain Node Explorer and Information - Track node rewards, age, version, APRs, slashes, and more.',
		tags: ['Analytics']
	},
	{
		title: 'Flipside Crypto',
		url: 'https://flipsidecrypto.xyz/',
		logo: '/images/services/flipside.jpeg',
		description:
			'Flipside crypto is a community of data analysts that publish data queries, dashboards, and datasets to answer questions that are important to protocols - including THORChain.',
		tags: ['Analytics']
	},
	{
		title: 'THORCharts',
		url: 'https://thorcharts.org/',
		logo: '/images/services/thorcharts.svg',
		description:
			'THORCharts is a useful dashboard for THORChain-related statistics and analytics including RUNE price, volume, liquidity, and more.',
		tags: ['Analytics']
	},
	{
		title: 'Nansen Portfolio',
		url: 'https://portfolio.nansen.ai/',
		logo: '/images/services/nansen.png',
		description:
			'Smart portfolio tracker with coverage of 476 protocols over 42 chains such as Ethereum, Thorchain, Cosmos, and more.',
		tags: ['Analytics']
	},
	{
		title: 'Pulsar Finance',
		url: 'https://app.pulsar.finance/',
		logo: '/images/services/pulsar.svg',
		description:
			'Pulsar Finance is a useful dashboard to track your liquidity positions on THORChain and elsewhere.',
		tags: ['Analytics']
	},
	{
		title: 'DeFi Yield',
		url: 'https://defiyield.app/',
		logo: '/images/services/defiyield.svg',
		description:
			'DeFi Yield is a useful dashboard to track your liquidity positions on THORChain and elsewhere.',
		tags: ['Analytics']
	},
	{
		title: 'THORChain Calendar Bot',
		url: 'https://twitter.com/THORCalendarBot',
		logo: '/images/services/calendarbot.jpeg',
		description:
			'THORChain Calendar Bot retweets THORChain-related Twitter Spaces. Turn on post notifications if you never want miss another live session.',
		tags: ['Analytics']
	},
	{
		title: '0xVentures',
		url: 'https://0xventures.org',
		logo: '/images/services/0xventures.png',
		description:
			'0xVentures is a DAO that functions as a fund with the sole purpose of investing in sector-disrupting projects. We are users and creators who deeply care about integrating with blockchains and their communities.',
		tags: ['Community', 'Other'],
	},
	{
		title: 'LP University',
		url: 'https://discord.gg/lpuniversity',
		logo: '/images/services/lp-university.png',
		description: 'Educational community discord server. Recommended to learn more about the basics of THORChain.',
		tags: ['Community', 'Other'],
	},
	{
		title: 'THORChads DAO',
		url: 'https://thorchads.com/',
		logo: '/images/services/thorchad.png',
		description:
			'The rewards programme and home of the THORChads Metaverse, where the community can enjoy creative projects such as NFTs, Merch drops, IDO access and games. THORChads.com acts as a creative launchpad, supporting new THORChain community projects.',
		tags: ['NFT', 'Community', 'Launchpad'],
	},
	{
		title: 'Nine Realms',
		url: 'https://twitter.com/ninerealms_cap',
		logo: '/images/services/ninerealms.jpeg',
		description:
			'Providing institutional liquidity and supporting development efforts on THORChain.',
		tags: ['Organization']
	},
	{
		title: 'RUNEBase.org',
		url: 'https://runebase.org',
		logo: '/images/services/runebase.png',
		description:
			'RUNEBase is a news and resource site offering news, guides, and a podcast on the THORChain ecosystem.',
		tags: ['Media'],
	},
	{
		title: 'ThorGuards',
		url: 'https://thorguards.com/',
		logo: '/images/services/thorguards.png',
		description:
			'ThorGuards is the community hub of THORChain, building off of ecosystem partners and including them in the traits. The 3D art features a unique fusion of Norse mythology and Viking elements with cyberpunk aesthetics and accents.',
		tags: ['NFT', 'Community'],
	},
	{
		title: 'Qi Capital',
		url: 'https://www.qicapital.org',
		logo: '/images/services/qicapital.png',
		description:
			'Qi Capital is a tightly knit community of experienced crypto investors and traders who work together to discover new gems and trends and help young teams grow and thrive.',
		tags: ['Community']
	},
	{
		title: 'THORChain Alerts Telegram',
		url: 'https://t.me/thorchain_alert',
		logo: '/images/services/tcalert.jpeg',
		description:
			'Telegram bot to monitor major events on THORChain.',
		tags: ['Tool']
	},
//	{
//		title: 'THORNOOB',
//		url: 'https://twitter.com/THORNOOBs',
//		logo: '/images/services/thornoob.png',
//		description:
//			'Covers Bi-Weekly Ecosystem Recaps, THORChain Updates, Interesting Facts and More.',
//		tags: ['Community']
//	},
	{
		title: 'Vanir Threads',
		url: 'https://vanirthreads.com',
		logo: '/images/services/vanirthreads.png',
		description:
			'Vanir Threads creates NFTs which are exchangeable for handmade physical collectibles - including the THOR Force 1, a THORChain branded, handmade custom sneaker set',
		tags: ['NFT']
	},
	{
		title: 'Immunefi',
		url: 'https://immunefi.com/bounty/thorchain/',
		logo: '/images/services/immunefi.svg',
		description:
			'Immunefi is the 3rd party bug bounty provider for THORChain. Report vulnerabilities and receive rewards for responsible disclosure.',
		tags: ['Bounty']
	},
	{
		title: 'THORChad Yourself',
		url: 'https://thorchad.glitch.me',
		logo: '/images/services/your-thorchad.png',
		description:
			'A simple bot to add the green THORChad ring around your profile picture for twitter, discord, or telegram.',
		tags: ['Other']
	},
	{
		title: 'GrassRoots Crypto',
		url: 'https://youtube.com/c/GrassRootsCrypto/',
		logo: '/images/services/grc.jpeg',
		description:
			'GrassRoots Crypto provides education services in the Blockchain and Crypto space via YouTube. Currently focusing on THORChain brining understanding to one of the most complex Defi Projects.',
		tags: ['Media']
	},
	{
		title: 'THORYield',
		url: 'https://app.thoryield.com/dashboard',
		logo: '/images/services/thoryield.jpeg',
		description:
			'With THORYield app you can Track your Liquidity on THORChain, view your wallet balances and staking accounts.',
		tags: ['Tool', 'Analytics']
	},
	{
		title: 'Thorstarter',
		url: 'https://thorstarter.org',
		logo: '/images/services/thorstarter.svg',
		description:
			'Thorstarter is a multichain Venture DAO and IDO platform that combines a unique launchpad model with liquidity grants to incubate, fund, and launch the most promising projects across DeFi.',
		tags: ['Launchpad']
	},
	{
		title: 'THORSwap Discord',
		url: 'https://discord.gg/thorswap',
		logo: '/images/services/discord.svg',
		description:
			'THORSwap Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'XDEFI Discord',
		url: 'http://discord.gg/xdefiwallet',
		logo: '/images/services/discord.svg',
		description:
			'XDEFI Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'THORWallet Discord',
		url: 'https://discord.gg/NkkyVayNy6',
		logo: '/images/services/discord.svg',
		description:
			'THORWallet Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'Defispot Discord',
		url: 'https://discord.gg/defispot',
		logo: '/images/services/discord.svg',
		description:
			'Defispot Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	}
];

export const updates = [
	{

        url: 'https://www.youtube.com/watch?v=b3OhOzvTKv4',

        title: 'How To Get THORChain Information & Dev Updates',

        thumbnail: '/images/media/thorchaininfo.jpeg'

    },

	{

        url: 'https://www.youtube.com/watch?v=CZLWT2lyiRU',

        title: 'Core Developer Explains How THORChain Works',

        thumbnail: '/images/media/edgediscussion.png'

    },
	
	{

        url: 'https://www.youtube.com/watch?v=gkNBl5xKfqQ',

        title: 'Cross Chain Panel: Quantum Miami 2023',

        thumbnail: '/images/media/quantum2023.jpeg'

    },

    {

        url: 'https://www.youtube.com/watch?v=vFQs8ML4qWE',

        title: 'Charlie Shrem: Untold Stories with Chad Barraford & Erik Voorhees',

        thumbnail: '/images/media/untold.png'

    },

	{
		url: 'https://youtube.com/playlist?list=PLMQ_o57NED-eh9eWxD-qqIvmdgXmW-6eX',

        title: 'THORChain Weekly Update Twitter Spaces',

        thumbnail: '/images/media/spaces.png'

    },

    {

        url: 'https://crypto101podcast.com/podcasts/ep-492-the-history-of-thorchain-a-bullish-case-for-regulation/',

        title: 'Crypto 101 Podcast: The History of THORChain',

        thumbnail: '/images/media/crypto101chad.png'

    },

    {

        url: 'https://crypto101podcast.com/podcasts/ep-439-executing-true-financial-inclusion-with-marcel-harmann-of-thorwallet-dex/',

        title: 'Crypto 101 Podcast: Marcel Harmann',

        thumbnail: '/images/media/crypto101marcel.png'

    },

    {

        url: 'https://bcdialogues.com/2023/01/01/ep-49-interview-chad-barraford-technical-lead-thorchain/',

        title: 'Blockchain Dialogues Episode #49: Chad Barraford',

        thumbnail: '/images/media/bcdialogue.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=pv_MS9DZz1s',

        title: 'Edge Wallet and THORChain: Savers and DEX Aggregation',

        thumbnail: '/images/media/thorchainedgespace.png'

    },

    {

        url: 'https://smarteconomypodcast.com/episode/db6986b8/chad-barraford-thorchain-empowering-users-through-decentralized-finance',

        title: 'Smart Economy Podcast: Chad Barraford',

        thumbnail: '/images/media/smarteconomy.png'

    },

    {

        url: 'https://cointelegraph.com/news/bitcoin-should-become-the-foundation-for-defi',

        title: 'Bitcoin could become the foundation of DeFi',

        thumbnail: '/images/media/bitcoinfoundation.png'

    },

    {

        url: 'https://financialit.net/news/cryptocurrencies/thorchain-enables-defi-bitcoin-breakthrough-single-sided-staking-service',

        title: 'THORChain Enables DeFi on Bitcoin',

        thumbnail: '/images/media/financialit.png'

    },

	{

        url: 'https://medium.com/thorchain/trust-wallet-integrates-thorchain-as-cross-chain-swap-provider-d56e01f1c146',

        title: 'Trust Wallet Integrates THORChain',

        thumbnail: '/images/media/trustwallet.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=1r9lG5D6Q6I',

        title: 'Digital Cash Network Podcast: Chad Barraford',

        thumbnail: '/images/media/digitalcashnetwork.png'

    },

    {

        url: 'https://blockworks.co/news/native-bitcoin-in-defi-this-dex-wants-to-boost-trust',

        title: 'Native Bitcoin in DeFi',

        thumbnail: '/images/media/blockworks1.png'

    },

    {

        url: 'https://www.crowdfundinsider.com/2022/12/200068-thorchain-integrates-with-trust-wallet-to-accelerate-adoption-of-crypto-self-custody/',

        title: 'THORChain Integrates with Trust Wallet',

        thumbnail: '/images/media/cfi.png'

    },

    {

        url: 'https://www.crowdfundinsider.com/2022/10/197130-thorchain-integrates-with-avalanche-to-support-multichain-interoperability/',

        title: 'THORChain Integrates with Avalanche',

        thumbnail: '/images/media/cfi2.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=5ClzXZN9fDM',

        title: 'CryptoCoinShow: Marcel Harmann, Founder of THORWallet DEX',

        thumbnail: '/images/media/ccshow.png'

    },

    {

        url: 'https://appdevelopermagazine.com/thorwallet-noncustodial-defi-wallet-review-with-marcel-harmann/',

        title: 'THORWallet review with Marcel Harmann',

        thumbnail: '/images/media/appdevmag.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=CgYpsfCNwiE',

        title: 'Bridging the Gap Among Blockchains',

        thumbnail: '/images/media/thegap.png'

    },  


    {

        url: 'https://medium.com/thorchain/thorchain-savers-vaults-fc3f086b4057',

        title: 'Savers Vaults Live on THORChain',

        thumbnail: '/images/media/savers.png'

    },

    {

        url: 'https://medium.com/thorchain/thorchain-integration-of-avalanche-c-chain-complete-de8786ac7435',

        title: 'Integration of Avalanche C-Chain Complete',

        thumbnail: '/images/media/avax.png'

    },

    {

        url: 'https://medium.com/thorchain/edge-wallet-integrates-thorchain-f85a69f7e8e6',

        title: 'Edge Wallet Integrates THORChain',

        thumbnail: '/images/media/edgetc.png'

    },

    {

        url: 'https://medium.com/thorchain/thorchain-tokenomics-what-is-rune-52d339633260',

        title: 'THORChain Tokenomics — What is RUNE?',

        thumbnail: '/images/media/tokenomics.png'

    },

    {

        url: 'https://medium.com/thorchain/atom-trading-live-on-thorchain-mainnet-7d13d5d8544f',

        title: 'ATOM Live on THORChain Mainnet',

        thumbnail: '/images/media/atom.png'

    },

    {

        url: 'https://medium.com/thorchain/thorchains-layers-of-security-e308d537acf1',

        title: 'THORChain Layers of Security',

        thumbnail: '/images/media/security.png'

    },

    {

        url: 'https://www.coindesk.com/video/recent-videos/consensus-2022-foundations-thorchain/',

        title: 'Consensus 2022 Foundations: THORchain',

        thumbnail: '/images/media/consensus2022.png'

    },

    {

        url: 'https://www.binance.com/hi/live/video?roomId=2104136',

        title: 'Binance Live: Mainnet Achievement AMA',

        thumbnail: '/images/media/binancelive.png'

	}
];

export const devtools = [

	{

        url: 'https://docs.thorchain.org/',

        title: 'THORChain Network Documentation',

        thumbnail: '/images/media/mainnet.jpeg'

    },

	{

        url: 'https://www.youtube.com/watch?v=Qowrasst2UQ',

        title: 'Develop on THORChain - Video Guide',

        thumbnail: '/images/media/developguide.jpeg'

    },

	{

        url: 'https://discord.gg/tW64BraTnX',

        title: 'THORChain Developer Discord',

        thumbnail: '/images/media/discord.png'

    },

	{

        url: 'https://dev.thorchain.org',

        title: 'THORChain Developer Docs + Quickstart Guides',

        thumbnail: '/images/media/mainnet.jpeg'

    },

	{

        url: 'https://xchainjs.org',

        title: 'XChainJS - Multichain Library',

        thumbnail: '/images/media/xchainjs.png'

    },

];